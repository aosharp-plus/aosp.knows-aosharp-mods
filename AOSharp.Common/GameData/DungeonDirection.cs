﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Common.GameData
{
    public enum DungeonDirection
    {
        None,
        Up,
        Down
    }
}
